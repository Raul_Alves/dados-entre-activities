package com.example.dadosentreactivities;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {

    private TextView nome;

    private  TextView idade;

    private TextView altura;

    String recebenome;

    Integer recebeidade;

    double recebealtura;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity2_main);

        nome = findViewById(R.id.txt_nome);
        recebenome = (String) getIntent().getSerializableExtra("nome");
        nome.setText(recebenome);

        idade = findViewById(R.id.txt_idade);
        recebeidade = (Integer) getIntent().getSerializableExtra("idade");
        idade.setText(String.valueOf(recebeidade));

        altura = findViewById(R.id.txt_altura);
        recebealtura = (Double) getIntent().getSerializableExtra( "altura");
        altura.setText(String.valueOf(recebealtura));
    }
}