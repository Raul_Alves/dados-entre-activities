package com.example.dadosentreactivities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText nome;

    private EditText idade;

    private EditText altura;
    private Button button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = findViewById(R.id.button);
        nome = findViewById(R.id.name);
        idade = findViewById(R.id.idade);
        altura = findViewById(R.id.altura);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String meu_nome = nome.getText().toString();
                String minha_idade = idade.getText().toString();
                String minha_altura = altura.getText().toString();

                Intent intent = new Intent (MainActivity.this, MainActivity2.class);

                intent.putExtra("nome", meu_nome);
                intent.putExtra("idade", minha_idade);
                intent.putExtra("altura", minha_altura);

                startActivity(intent);
                }
        });

    }

}